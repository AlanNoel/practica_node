const http = require("http");

const server = http.createServer((request, response) => {
  console.log(request.url);
  if (request.url === "/") {
    response.write("Welcome to the server");
    return response.end();
  }
  if (request.url == "/about") {
    response.write("acerca DE");
    return response.end();
  }
  response.write("404");
  response.end();
});

server.listen(3001);

console.log("Server ON 3001");
 